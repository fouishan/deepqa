# **Link to original repository:** #

[https://github.com/Conchylicultor/DeepQA](https://github.com/Conchylicultor/DeepQA)


# ** Changes made by me: ** #

The original rep has used 4 data sources. From that, I've selected scotus and deleted other folders because I could fit in the QnA data in that format. The format is 

question:what is dividend?
answer:dividend is...

The data file is in folder scotus. It's name is scotus with no file extension

I've pre-processed the QnA file removing the stop words and some punctuations

Currently, I'm getting no output in the interactive mode 
![pastedImage.png](https://bitbucket.org/repo/x899xdB/images/252021245-pastedImage.png)


To run this use the following command 

python main.py --test interactive 

check the original readme for more details